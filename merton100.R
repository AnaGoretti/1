#!/usr/bin/env Rscript
rm(list=ls())

setwd('~/Desktop/modelos_de_credito/1')
# 6 anos 
# 100 empresas
emp <- read.csv('stocks2.csv',header = T, stringsAsFactors = FALSE)
emp <- emp[,-1]

datos <- read.csv('liab_divs2.csv',header = T, stringsAsFactors = FALSE)
datos <- datos[,2:102]
rownames(datos) <- datos$variables

Liabilitie <- as.numeric(datos["TotalLiabilities", -1])
CurrentL <- as.numeric(datos["CurrentLiabilities", -1])
LongL <- Liabilitie - CurrentL
last <- nrow(emp)
Dinicial <-  as.numeric(datos["Dividends", -1])

G <- as.numeric(datos["DividendsGrowth", -1])
C <- 0.03
R <- 0.01

cambio <- seq(from = 1, to = nrow(emp), by = round(nrow(emp)/6))
probabilities <- data.frame(matrix(data=NA, nrow = length(cambio)-1, ncol = ncol(emp)))

  p <- c()
  for(m in 2:95){
  L <- Liabilitie[m]
  CL <- CurrentL[m]
  LTL <- L -CL
  time <- (0.5*CL + 10 * LTL)/L
  D0 <- Dinicial[m]
  g <- G[m]
  c <- 0.03
  r <- 0.01
  print(m)
  if(m >= 95) next;
  print(m)
  
  aux <- 0
  for(j in seq(from = 1, to = nrow(emp), by = round(nrow(emp)/6))){
    incremento <- round(nrow(emp)/6)
    aux <- aux + 1 
    if(aux>6) next;
    if(j == cambio[length(cambio)]){incremento <- nrow(emp)-j}
    E <- emp[j:(j+incremento), m]
    D <- 0
    I <- 0
    for (i in 1:floor(time)){
    D <- D + D0 * (1 + g)^i * exp(r *( time - i))
    I <- I + L * c * exp(r *( time - i))
    }
    ndays <- 252
    
    A <- E + L
    
    sigma_E <- sd(diff(log(E)))*sqrt(ndays)
    sigma_A <- sd(diff(log(A)))*sqrt(ndays)
    
    d_1 <- (log(A/(L + D + I)) + (r + sigma_A^2)*(ndays))/(sigma_A)
    d_2 <- d_1 - sigma_A
    
    k_1 <- (log(A/(I + D))+(r + sigma_A^2/2))/(sigma_A)
    k_2 <- k_1 - sigma_A
    
    phi <- function(x) pnorm(x)
    A_n <- (E + (L + D + I)*exp(-r)*phi(d_2) - (D + I)*exp(-r)*phi(k_2))/(phi(d_1)+(1-phi(k_1))*(D/(D+I)))
    ct <- 0
    while (sum((A-A_n)^2) > 0.007){
      sigma_A_n <- sd(diff(log(A_n)))*sqrt(ndays)
      
      d_1 <- (log(A_n/(L + I + D))+(r + sigma_A_n^2/2))/(sigma_A_n)
      d_2 <- d_1 - sigma_A_n
      
      k_1 <- (log(A_n/(I + D)) + (r + sigma_A_n^2/2))/(sigma_A_n)
      k_2 <- k_1 - sigma_A_n
      
      A <- A_n
      A_n <- (E + (L + D + I)*exp(-r)*phi(d_2) - (D + I)*exp(-r)*phi(k_2))/(phi(d_1)+(1-phi(k_1))*(D/(D+I)))
      if(any(is.na(A_n)) || any(is.nan(A_n)) == T || any(A_n) <= 0) break;
      if (ct > 1000) break;
      ct <- ct + 1
    }
  
  ndays <- time * 262
  
  sigma_A <- (sd(diff(log(A)))*sqrt(ndays))
  
  mu_A <- (mean(diff(log(A)))*ndays)
  
  lA_T <- rnorm(100000,log(A_n[length(A_n)]) - ( mu_A - sigma_A^2/2),sigma_A)
  
  p[aux] <- length(lA_T[lA_T < log(L + D + I)])/ length(lA_T)
  
    } #a?os
  
  probabilities[, m] <- 1 - (1-p)^(1/time)  
  
  } #empresas
 probabilities <- probabilities[colSums(!is.na(probabilities)) > 0]
  #probabilities <- na.omit(probabilities)
  
  #Probabilidad Defaul
  
  default_prob <- probabilities
  
states <- as.data.frame(default_prob)
limits <- c(0.0018, 0.0028, 0.01, 0.0211, 0.0822, 0.3124, 1)
grades <- c('AA', 'A', 'BBB', 'BB', 'B', 'CCC')
  
  states[states < limits[1]] <- 'AAA'
  states[states == 1]        <- 'Default'

  for(index in 1:(length(limits)-1)){
    
    states[ states >= limits[index] & states < limits[index+1] ] <- grades[index]
    
  }
  
  for(index in 1:ncol(states)){
    if(sum(states[, index] == 'Default') >= 1 ){
      position <- which(states[, index] == 'Default')
      states[min(position):nrow(states), index] <- 'Default'
    }
  }

# Matriz de transicion
  states <- t(states)
  pair_reloaded <- function(line, initial_class, given_class){
    a <- states[line, -ncol(states)]  # Ultimo vector
    b <- states[line, -1]             # Primer vector
    count <- length(intersect(which(a == given_class), which(b == initial_class)))
    return(count)
  }
  
  grades <- c('AAA','AA','A','BBB','BB','B','CCC','Default')
  
  total <- numeric(length(grades))
  
  names(total) <- as.character(grades)
  
  for(index in 1:length(grades)){
    
    total[index] <- sum(states[, -ncol(states)] == grades[index])
    
  }
  
  combinations <- merge(grades, grades)
  
  probs_notequal <-  numeric(nrow(combinations)) 
  names <- numeric()
  
  for(index in 1:nrow(combinations)){
    initial_class <- as.character.factor(combinations[index, 1])
    given_class   <- as.character.factor(combinations[index, 2])
    names[index]  <- paste(initial_class, given_class, sep = ' | ')
    
    for(line in 1:nrow(states)){
      
      probs_notequal[index] <- probs_notequal[index] + pair_reloaded(line,
                                                                     initial_class,
                                                                     given_class)
    }
    
  }
  
  names(probs_notequal) <- names
  prob_df <- data.frame(matrix(NA, nrow = length(grades), ncol = length(grades)))
  colnames(prob_df) <- as.character(grades)
  rownames(prob_df) <- as.character(grades)
  
  for(index in 1:length(probs_notequal)){
    initial_class <- as.character.factor(combinations[index, 1])
    given_class   <- as.character.factor(combinations[index, 2])
    names  <- paste(initial_class, given_class, sep = ' | ')
    
    prob_df[given_class, initial_class] <- probs_notequal[names] / total[given_class]
    
  }

  
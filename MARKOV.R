#!/usr/bin/env Rscript
setwd('~/Desktop/modelos_de_credito/1')
df <- read.csv('markov_chain.csv', head=T)

estados<-matrix( 0, nrow= 600, ncol= 19)

#El primer estado de todos los clientes es NC = (-1)
estados[,1]<--1

for (i in 1:600 ){  #renglones matrix estados
  for (j in seq(2,36,2)){ #columnas df y estados  
           if (df[i,j-1] != df[i,j] & df[i,j]>0 )
      {
          estados[i,(j/2)+1]= 0
      }else
      {
          if(df[i,j-1] == df[i,j])
            {
              estados[i,(j/2)+1]= -1 
            }else
                {
                  if(df[i,j-1] !=0 & df[i,j]==0){
                      if(estados[i,(j/2)]>= 0)
                  {
                    estados[i,(j/2)+1]= estados[i,(j/2)]+1
                      }
                    if (estados[i,(j/2)+1]>=3){
                      estados[i,(j/2)+1]=3
                    }
                  }
                }
      }
  }
  }

#Calcular Probabilidades
s<-c(-1,0,1,2,3) #estados

M<-matrix( 0, nrow=5, ncol= 5) #numerador

N<-c(0,0,0,0,0) #denominador

for(a in 1:5){
for(j in 1:600){
  for(i in 1:18){
    if( estados[j,i]== s[a]){
      N[a]<-N[a]+1
    }
    for (b in 1:5){
      if (estados[j,i]== s[a] && estados[j,i+1]== s[b])
      {
        M[a,b]<-M[a,b]+1
      }
     }
    }
  }
}

P<-matrix( 0, nrow=5, ncol= 5) #matriz de probabilidades
x<-0
for( x in 1:5){
  P[x,]<-M[x,]/N[x]
} 
  
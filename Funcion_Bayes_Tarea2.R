#Tarea#2 
#del 15 al 21 de Febrero
#Funcion que calcula probabilidades
#!/usr/bin/env Rscript

setwd('~/Desktop/modelos_de_credito/1')
df<- read.table('german.data')

source('transform_data.r')

varnames <- c('checking_acc','time_credit_acc','credit_history',
           
              'purpose','amount','savings_acc','p_employment_time',
              
              'installment_rate','marita_status_sex','other_debtors',
              
              'p_residence_time','property','age','other_installment',
              
              'housing','number_of_credits','job','dependants','has_phone',
            
              'foreign_worker','is_good')
colnames(df)<-varnames

df<-transform_data(df)

vars_model<- c('amount',
               
               'age',
               
               'credit_history_numeric',
               
               'purpose_numeric',
               
               'p_employment_time_numeric',
               
               'checking_acc_numeric'
)

set.seed(123456)

#crear una muestra del 60% delos datos

selected_rows <- sort(sample(nrow(df),floor(0.6*nrow(df)))) #crea una muestra del 60% de los datos
data_model<-df[selected_rows,c(vars_model,'is_good')]
data_test<-df[-selected_rows,c(vars_model,'is_good')]
hist_tot<-hist(data_model[,'age'],plot=F)

breaks_age<-hist_tot$breaks

good_model<-data_model[data_model$is_good==1,] #df de los que son buenos

bad_model<-data_model[data_model$is_good==2,] #df de los que son malo

p_age_l_G<- hist(good_model[,'age'],freq=F,breaks=breaks_age) #Probabilidad de la edad dad que sabemos q es bueno

p_age_l_B<- hist(bad_model[,'age'],freq=F,breaks=breaks_age) #Probabilidad de la edad dad que sabemos q es bueno

prob_age_l_G<-p_age_l_G$counts/sum(p_age_l_G$counts)

prob_age_l_B<-p_age_l_B$counts/sum(p_age_l_B$counts)

relative_prob_age<-prob_age_l_G/prob_age_l_B

plot(p_age_l_G$mids,prob_age_l_G)

plot(p_age_l_B$mids,prob_age_l_B)

plot(relative_prob_age)

bayes<-function(x){
  
   hist_tot<-hist(data_model[,x],plot=F)
  
   breaks_x<-hist_tot$breaks
  
   good_model<-data_model[data_model$is_good==1,] #df de los que son buenos
  
   bad_model<-data_model[data_model$is_good==2,] #df de los que son malo
  
   p_x_l_G<- hist(good_model[,x],freq=F,breaks=breaks_x) #Probabilidad de la edad dad que sabemos q es bueno
  
   p_x_l_B<- hist(bad_model[,x],freq=F,breaks=breaks_x) #Probabilidad de la edad dad que sabemos q es bueno

    prob_x_l_G<-p_x_l_G$counts/sum(p_x_l_G$counts)
  
    prob_x_l_B<-p_x_l_B$counts/sum(p_x_l_B$counts)
  
    relative_prob_x<-prob_x_l_G/prob_x_l_B
  
     plot(p_x_l_G$mids,prob_x_l_G)

    plot(p_x_l_B$mids,prob_x_l_B)


    plot(relative_prob_x)
    }

bayes('age')

bayes('amount')

bayes('credit_history_numeric')

bayes('purpose_numeric')

bayes('p_employment_time_numeric')

bayes('checking_acc_numeric')

